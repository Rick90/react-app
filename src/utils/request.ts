import Axios from 'axios';
import { message } from 'antd';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

const axios = Axios.create({
  baseURL: '/api/v1',
  timeout: 2000,
  headers: {
    'Content-Type': 'application/json',
  },
});

axios.interceptors.request.use((config: any) => {
  const token = localStorage.getItem('token');
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

axios.interceptors.response.use(
  (response) => {
    const { data, status } = response;
    if (status === 200) {
      return data;
    }
    message.error(`请求错误 ${response.statusText}: ${response}`);
    if (status === 401) {
      history.push('/login');
    }
    return Promise.reject(new Error(response.statusText || 'Error'));
  },
  (error) => {
    if (error.response && error.response.status) {
      switch (error.response.status) {
        case 401:
          history.push('/login');
          break;
        case 403:
          history.push('/login');
          break;
        case 404:
          history.push('请求不存在');
          break;
        default:
          message.error('请求错误');
      }
    }
    return Promise.reject(error);
  },
);

export default axios;
