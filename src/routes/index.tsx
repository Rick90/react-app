import React, { useCallback, Suspense, lazy } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

const BasicLayout = lazy(() => import('@components/layout/index'));
const Login = lazy(() => import('@pages/login/index'));
const NoMatch = lazy(() => import('@pages/noMatch/index'));

const RouterCom = (): JSX.Element => {
  /** 跳转到某个路由之前触发 * */
  const onEnter = useCallback((Component, property) => {
    const userTemp = localStorage.getItem('userinfo');
    if (userTemp) {
      return <Component {...property} />;
    }
    return <Redirect to="/login" />;
  }, []);

  return (
    <Router>
      <Suspense fallback={<div>Loading...</div>}>
        <Route
          render={(): JSX.Element => {
            return (
              <Switch>
                <Route exact path="/" render={(props): JSX.Element => onEnter(BasicLayout, props)} />
                <Route path="/login">
                  <Login />
                </Route>
                <Route path="*">
                  <NoMatch />
                </Route>
              </Switch>
            );
          }}
        />
      </Suspense>
    </Router>
  );
};
export default RouterCom;
