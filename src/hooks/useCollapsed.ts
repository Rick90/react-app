import { useState, useEffect } from 'react';

const useCollapsed = (): [boolean, (bool: boolean) => void] => {
  const [collapsed, setCollapsed] = useState(false);
  useEffect(() => {
    setCollapsed(!collapsed);
  }, []);
  return [collapsed, setCollapsed];
};
export default useCollapsed;
