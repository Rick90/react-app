type collpasedType = {
  [key: string]: boolean;
};

export const initialState = {
  collapse: false,
};

export const reducer = (state: collpasedType = initialState, action: string) => {
  switch (action) {
    case 'CLOSED':
      return {
        ...state,
        collapse: true,
      };
    case 'OPEN':
      return {
        ...state,
        collapse: false,
      };
    default:
      return state;
  }
};
