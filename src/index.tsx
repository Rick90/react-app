import React from 'react';
import ReactDOM from 'react-dom';
import Router from './routes';
import './app.scss';

if (module && module.hot) {
  module.hot.accept();
}

ReactDOM.render(<Router />, document.querySelector('#root'));
