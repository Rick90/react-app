import React from 'react';

const NoMatch = (): JSX.Element => {
  return <div className="no-match-page">页面没找到</div>;
};
export default NoMatch;
