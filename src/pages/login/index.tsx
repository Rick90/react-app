import React from 'react';
import { useHistory } from 'react-router-dom';
import { Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import API from '@api/index';
import logo from '@assets/images/logo.png';
import './index.scss';

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};

interface ILoginState {
  username: string;
  password: string;
}

const Login = (): JSX.Element => {
  const [form] = Form.useForm();
  const history = useHistory();
  const getTopicList = (data: any) => {
    API.getTopics(data).then((res) => {
      console.log(res);
      history.push('/');
      history.go();
    });
  };
  const onFinish = (value: ILoginState) => {
    localStorage.setItem('userinfo', value.username);
    getTopicList({ page: 1, limit: 10 });
  };
  return (
    <div className="login-container">
      <div className="login-panel">
        <h3 className="login-title">
          <img src={logo} alt="" />
          React Admin
        </h3>
        <Form {...layout} form={form} name="login-form" onFinish={onFinish}>
          <Form.Item name="username" label="用户名" rules={[{ required: true, message: '请输入用户名' }]}>
            <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="请输入用户名" />
          </Form.Item>
          <Form.Item name="password" label="密码" rules={[{ required: true, message: '请输入密码' }]}>
            <Input prefix={<LockOutlined className="site-form-item-icon" />} type="password" placeholder="请输入密码" />
          </Form.Item>
          <Form.Item className="login-form-button">
            <Button type="primary" htmlType="submit" className="login-btn">
              登录
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};
export default Login;
