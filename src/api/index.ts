import request from '@utils/request';

const API = {
  getTopics: (params: object) => request('/topics', params),
  getMenus: () => request('/menus'),
  getList: () => request('/list'),
};
export default API;
