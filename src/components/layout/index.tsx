import React, { useState, useEffect, useReducer } from 'react';
import { Layout, Menu } from 'antd';
import API from '@api/index';
import Icon from '@components/dynamicIcon';
import HeaderCom from '@components/header';
import MainContent from '@components/main';
import { initialState, reducer } from '../../reducers/collapsed';
import './layout.scss';

type IconType = keyof typeof Icon;
type MenuState = {
  id: number;
  title: string;
  parentId?: number;
  icon: IconType;
};

type SubMenuState = MenuState & {
  subMenus: Array<MenuState>;
};

const { SubMenu } = Menu;

const LeftMenu = (): React.ReactElement => {
  const [menus, handleMenus] = useState<Array<SubMenuState>>([]);
  const [menuKeys, setMenuKeys] = useState(['9']);
  const [state, dispatch] = useReducer(reducer, initialState);
  const getMenus = () => {
    API.getMenus().then((res: any) => {
      if (res.code === 200) {
        handleMenus(res.rows);
      }
    });
  };
  useEffect(() => {
    getMenus();
  }, [menus]);

  function openChange(keys: Array<string>) {
    setMenuKeys([keys[keys.length - 1]]);
  }

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Layout.Sider
        trigger={null}
        collapsible
        collapsed={state.collapse}
        onCollapse={() => dispatch(state.collapse ? 'CLOSED' : 'OPEN')}
      >
        <Menu
          theme="dark"
          mode="inline"
          openKeys={menuKeys}
          onOpenChange={(keys) => openChange(keys)}
          defaultSelectedKeys={menuKeys}
        >
          {menus.map((menu) => {
            return menu.subMenus.length ? (
              <SubMenu key={menu.id} title={menu.title} icon={<Icon name={menu.icon} />}>
                {menu.subMenus.map((sub) => {
                  return <Menu.Item key={sub.id}>{sub.title}</Menu.Item>;
                })}
              </SubMenu>
            ) : (
              <Menu.Item key={menu.id} icon={<Icon name={menu.icon} />}>
                {menu.title}
              </Menu.Item>
            );
          })}
        </Menu>
      </Layout.Sider>
      <Layout>
        <HeaderCom collapse={state.collapse} onDispatch={(action: string) => dispatch(action)} />
        <MainContent />
      </Layout>
    </Layout>
  );
};
export default LeftMenu;
