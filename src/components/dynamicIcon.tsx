import React, { useState, useEffect } from 'react';
import { LoadingOutlined } from '@ant-design/icons';

import * as AllIcons from '@ant-design/icons';

// eslint-disable-next-line no-unused-vars
type PickProps<T> = T extends (props: infer P1) => any ? P1 : T extends React.ComponentClass<infer P2> ? P2 : unknown;

type AllKeys = keyof typeof AllIcons;

type PickCapitalizeAsComp<K extends AllKeys> = K extends Capitalize<K> ? K : never;

type IconNames = PickCapitalizeAsComp<AllKeys>;

export type PickIconPropsOf<K extends IconNames> = PickProps<typeof AllIcons[K]>;

const Icon = <T extends IconNames, P extends Object = PickIconPropsOf<T>>({
  name,
  ...props
}: { name: T } & Omit<P, 'name'>) => {
  const [Comp, setComp] = useState<React.ClassType<any, any, any>>(LoadingOutlined);
  useEffect(() => {
    import(`@ant-design/icons/${name}.js`).then((mod) => {
      setComp(mod.default);
    });
  }, [name]);
  return <Comp {...props} />;
};
export default Icon;
