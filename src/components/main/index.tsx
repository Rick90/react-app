import React, { useState, useEffect } from 'react';
import { Table, Layout } from 'antd';
import { EChartOption } from 'echarts';
import API from '@api/index';
import Echart from './charts';

const { Content } = Layout;

const option: EChartOption = {
  tooltip: {
    formatter: '{a} <br/>{b} : {c}%',
  },
  grid: {
    left: '1%',
    right: '1%',
    top: '8%',
    bottom: '4%',
    containLabel: true,
  },
  series: [
    {
      type: 'gauge',
      name: '业务指标',
      radius: '80%',
      center: ['50%', '55%'],
      axisLine: {
        lineStyle: {
          width: 12,
        },
      },
      axisTick: {
        length: 15,
      },
      splitLine: {
        length: 20,
      },
      axisLabel: {
        backgroundColor: 'auto',
        borderRadius: 2,
        color: '#eee',
        padding: 3,
        textShadowBlur: 2,
        textShadowOffsetX: 1,
        textShadowOffsetY: 1,
        textShadowColor: '#ccc',
      },
      detail: { formatter: '{value}%' },
      data: [{ value: 66, name: '使用率' }],
    },
  ],
};

const ListComponent = (): React.ReactElement => {
  const [rows, setRows] = useState([]);
  const getRows = () => {
    API.getList().then((res: any) => {
      if (res.code === 200) {
        setRows(res.rows);
      }
    });
  };
  useEffect(() => {
    getRows();
  }, [rows]);

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Age',
      dataIndex: 'age',
      key: 'age',
    },
    {
      title: 'Address',
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: 'Tags',
      key: 'tags',
      dataIndex: 'tags',
    },
  ];
  return (
    <Content
      style={{
        padding: 24,
        margin: 20,
        minHeight: 280,
      }}
    >
      {/* <Table dataSource={rows} columns={columns} /> */}
      <Echart option={option} />
    </Content>
  );
};
export default ListComponent;
