import React, { useState } from 'react';
import { Layout, Avatar, Dropdown } from 'antd';
import { UserOutlined, MenuFoldOutlined, DownOutlined, MenuUnfoldOutlined, PoweroffOutlined } from '@ant-design/icons';
import Icon from '@components/dynamicIcon';
import { fullscreen, exitFullscreen } from '@utils/index';
import './header.scss';

const { Header } = Layout;
type HeaderType = {
  collapse: boolean;
  onDispatch: (action: string) => void;
};
const MenuContent = (
  <div className="popover-content">
    <div className="ls sign-out">
      <PoweroffOutlined style={{ fontSize: '14px', marginRight: '5px' }} />
      退出登录
    </div>
  </div>
);

const HeaderCom = (props: HeaderType): React.ReactElement => {
  const [isFullscreen, setIsFullscreen] = useState(false);
  const { collapse, onDispatch } = props;

  const handleFullscreen = () => {
    setIsFullscreen((isFull) => {
      if (isFull) {
        exitFullscreen();
      } else {
        fullscreen();
      }
      return !isFull;
    });
  };
  return (
    <Header>
      <div className="left">
        {collapse ? (
          <MenuUnfoldOutlined onClick={() => onDispatch('OPEN')} style={{ cursor: 'pointer', fontSize: '20px' }} />
        ) : (
          <MenuFoldOutlined onClick={() => onDispatch('CLOSED')} style={{ cursor: 'pointer', fontSize: '20px' }} />
        )}
      </div>
      <div className="right">
        <div>
          <Icon onClick={handleFullscreen} name={isFullscreen ? 'FullscreenOutlined' : 'FullscreenExitOutlined'} />
        </div>
        <div className="userinfo">
          <Dropdown overlay={MenuContent} placement="bottomCenter" arrow>
            <div>
              <Avatar style={{ backgroundColor: '#87d068' }} icon={<UserOutlined />} />
              <span className="username">Jimmy</span>
              <DownOutlined />
            </div>
          </Dropdown>
        </div>
      </div>
    </Header>
  );
};
export default HeaderCom;
