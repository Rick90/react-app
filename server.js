const express = require('express');
const http = require('http');
const path = require('path');
const webpack = require('webpack');
const devMiddleware = require('webpack-dev-middleware');
const hotMiddleware = require('webpack-hot-middleware');
const { createProxyMiddleware } = require('http-proxy-middleware');
const chalk = require('chalk');
const open = require('open');
const dotenv = require('dotenv');
dotenv.config({ path: '.devenv' });
const config = require('./build/webpack.dev.conf');

const app = express();
const server = http.createServer(app);
const PORT = process.env.PORT || 9000;
const DIST_DIR = path.join(__dirname, 'dist');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const complier = webpack(config);
const pattern = /^(?!\/?api).+$/;

app.use(
  devMiddleware(complier, {
    index: false,
    publicPath: config.output.publicPath,
  }),
);
app.use(hotMiddleware(complier));

app.use(pattern, (req, res, next) => {
  const filename = path.join(DIST_DIR, 'index.html');
  complier.outputFileSystem.readFile(filename, (err, result) => {
    if (err) {
      return next(err);
    }
    res.set('content-type', 'text/html');
    res.send(result);
    res.end();
  });
});

// 接口转发
app.use(
  '/api',
  createProxyMiddleware({
    target: 'http://localhost:3000',
    changeOrigin: true,
  }),
);

server.listen(PORT, () => {
  console.log(chalk.cyan(`server listening on PORT ${PORT}`));
  console.log(chalk.yellowBright('Press Ctrl+C to quit'));
  open(`http:localhost:${PORT}`);
});
