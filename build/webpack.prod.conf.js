const { merge } = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const PurgeCSSPlugin = require('purgecss-webpack-plugin');
const glob = require('glob');
const TerserPlugin = require('terser-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const config = require('./webpack.base.conf');
const paths = require('./paths');

module.exports = merge(config, {
  mode: 'production',
  devtool: false,
  target: 'web',
  output: {
    filename: 'js/[name].[contenthash:8].js',
    path: paths.build,
    publicPath: '/',
    assetModuleFilename: 'images/[name].[contenthash:8].[ext]',
    pathinfo: false,
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: 'css/[name].[contenthash:8].css',
      chunkFilename: 'css/[name].[contenthash:8].chunk.css',
    }),
    new PurgeCSSPlugin({
      paths: glob.sync(`${paths.src}/**/*`, { nodir: true }),
    }),
  ],
  optimization: {
    sideEffects: true,
    usedExports: true,
    runtimeChunk: true,
    minimize: true,
    moduleIds: 'deterministic',
    minimizer: [
      new TerserPlugin({
        extractComments: false,
        parallel: true,
      }),
      new CssMinimizerPlugin({
        parallel: 4,
      }),
    ],
    splitChunks: {
      chunks: 'all',
      minSize: 0,
      minChunks: 1,
      cacheGroups: {
        vendors: {
          // node_modules里的代码
          test: /[\\/]node_modules[\\/]/,
          chunks: 'all',
          // name: 'vendors', 一定不要定义固定的name
          priority: 10, // 优先级
          enforce: true,
        },
      },
    },
  },
});
