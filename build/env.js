const isProduction = process.env.NODE_ENV === 'proodcution'
const isDevelopment = process.env.NODE_ENV === 'development'

module.exports = {
  isProduction,
  isDevelopment
}
