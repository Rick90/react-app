const path = require('path');
const fs = require('fs');

const appDirectoryName = fs.realpathSync(process.cwd());

const resolvePaths = (relativePath) => path.resolve(appDirectoryName, relativePath);
const fileExtensions = ['ts', 'tsx', 'js', 'jsx'];
const resolveModule = (resolveFn, filePath) => {
  const extension = fileExtensions.find((ex) => fs.existsSync(resolveFn(`${filePath}.${ex}`)));
  if (extension) {
    return resolveFn(`${filePath}.${extension}`);
  }
  return resolveFn(`${filePath}.ts`);
};
console.log(resolvePaths('public'));
module.exports = {
  build: resolvePaths('dist'),
  src: resolvePaths('src'),
  public: resolvePaths('public'),
  entryFile: resolveModule(resolvePaths, 'src/index'),
  components: resolvePaths('src/components'),
  utils: resolvePaths('src/utils'),
  hooks: resolvePaths('src/hooks'),
  assets: resolvePaths('src/assets'),
  pages: resolvePaths('src/pages'),
  models: resolvePaths('src/models'),
  store: resolvePaths('src/store'),
  api: resolvePaths('src/api'),
  appHtml: resolvePaths('public/index.html'),
  modules: resolvePaths('node_modules'),
  tsConfig: resolvePaths('tsconfig.json'),
  cacheDir: resolvePaths('.temp_cache'),
};
